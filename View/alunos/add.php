


<?php

require '../../kint/Kint.class.php';
//ddd ($_GET);

/* codigo de erros/sucessos no submit */




$con = new PDO("mysql:host=127.0.0.1;dbname=escola","root","");

$stmt = $con->prepare("select * from Estado");
$stmt->execute();
$opcoesEstado = "";
while ($linhadoBancoDeDados = $stmt->fetch(PDO::FETCH_OBJ)) {
  $opcoesEstado .="<option value='".$linhadoBancoDeDados->id."'>".$linhadoBancoDeDados->descricao."</option>";
}
?>


<html>
<head>
  <title>Cadastro Aluno</title>
  <meta charset="utf-8">
  <script type="text/javascript" src="../../js/funcaos.js"></script> 
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> <!--Import Google Icon Font-->
  <link type="text/css" rel="stylesheet" href="../../css/materialize.min.css"  media="screen,projection"/>  <!--Import materialize.css-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/> <!--Let browser know website is optimized for mobile--> 
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>  <!--Import jQuery before materialize.js-->
  <script type="text/javascript" src="../../js/materialize.min.js"></script>  <!--Import jQuery before materialize.js-->
</head>

<body>
  

<?php include"../masterPages/nav.php";?>

  

</head>
<body>
  <br/>
  <div class="container">
   



    <div class="card-panel cyan">Cadastro de Usuário</div>



    <form action="../../Controller/alunos/AlunosController.php" method="post" class="col s12" enctype="multipart/form-data" name="file">
      
     <div class="row">
     
        
          <div class="input-field col s6">
            <i class="material-icons prefix">account_circle</i>
            <input id="icon_prefix" type="text" class="validate" name="txtNome">
            <label for="icon_prefix">Nome Completo</label>
          </div>
          
          <div class="input-field col s6">
            <i class="material-icons prefix">mail</i>
            <input id="email" type="email" class="validate" name="txtEmail">
            <label for="email">Email</label>
          </div>
    

      
        <div class="input-field col s6">
          <i class="material-icons prefix">phone</i>
          <input id="icon_prefix" type="tel" class="validate" name="txtTel">
          <label for="icon_prefix">Telefone</label>
        </div>
        <div class="input-field col s6">
          <i class="material-icons prefix">assignment_ind</i>
          <input id="icon_telephone" type="tel" class="validate" name="txtCPF">
          <label for="icon_telephone">Cpf</label>
        </div>

        <div class="input-field col s6">
          
          <select class="browser-default" name="estadoCivil">
            <option value="-1" disabled selected>Selecione o Estado Civil</option>
            <option value="1">Solteiro</option>
            <option value="2">Casado</option>
            <option value="3">Viúvo</option>
            <option value="4">Divorciado</option>
          </select>
        </div>

         <div class="input-field col s6">
          <select class="browser-default" name="sexo">
            <option value="-1" disabled selected>Selecione o Sexo</option>
            <option value="1">Masculino</option>
            <option value="2">Feminino</option>
          </select>
        </div>


        <div class="input-field col s6">
         
          <select class="browser-default" onchange="getCities();" name="estado" id="comboEstado">
            <option value="-1" disabled selected>Selecione o Estado</option>
            <?php echo $opcoesEstado  ?>
          </select>
        </div>


        <div class="input-field col s6">
          <select class="browser-default"  name="cidade" id="comboCidade">
            <option value="-1" disabled selected>Selecione o Estado</option>
          </select>
        </div>



<div class="input-field col s6">
    <div class="file-field input-field">
      <div class="btn">
        <span>File</span>
        <input type="file" name="file">
      </div>
      <div class="file-path-wrapper">
        <input class="file-path validate" type="text">
      </div>
    </div>
  </div>

 </form>



   <div class="input-field col s6">
               
      <button class="btn waves-effect waves-light" type="submit" name="action">Submit
        <i class="material-icons right">send</i>
      </button>
        </div>

</div>    
  </div>


<?php include"../masterPages/rodape.php";?>



  </body>
  </html>



  
  
   <script> 
          $(document).ready(function(){
            

            Materialize.toast('<?php echo $_GET["strMsg"]; ?>', 4000);

          });
          </script>
