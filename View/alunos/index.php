<html >
<head>
	<title>Menu Principal</title>
	<meta charset="utf-8">


<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="../../css/materialize.min.css"  media="screen,projection"/>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="../../js/materialize.min.js"></script>


</head>
<body bgcolor="#212121">


<?php include"../masterPages/nav.php";?>
<br>
<div class="container">

	<div class="slider">
		<ul class="slides">
			<li>
				<img src="http://i.imgsafe.org/ce5f242.png" width="100"> <!-- random image -->
				<div class="caption center-align">
					<h3>Seja Bem Vindo</h3>
					<h5 class="light grey-text text-lighten-3">A Melhor Empresa do Brasil!.</h5>
				</div>
			</li>
		
			<li>
				<img src="http://thumbs.dreamstime.com/z/sala-de-aula-3332533.jpg"> 
				<div class="caption right-align">
					<h3 class="light black grey-text text-lighten-3">Oportunidade aos Alunos?</h3>
					<h5 class="light black grey-text text-lighten-3">Você só encontra aqui!</h5>
				</div>
			</li>
			<li>
				<img src="http://www.brumadoagora.com.br/hd-imagens/arquivos/material-escolar-67-Brumado-Agora.jpg"> 
				<div class="caption center-align">
					<h3 class="light black accent-4-text text-lighten-3">Ajudando o Aluno a ter materiais escolares!</h3>
					<h5 class="light black accent-4-text text-lighten-3">Preços Acessiveis.</h5>
				</div>
			</li>
		</ul>
	</div>
</div>
<?php include"../masterPages/rodape.php";?>



		</body>
		</html>
		<script> 
		$(document).ready(function(){
			$('.slider').slider();
		});
		</script>