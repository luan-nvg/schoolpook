 <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> <!--Import Google Icon Font-->
  <link type="text/css" rel="stylesheet" href="../../css/materialize.min.css"  media="screen,projection"/>  <!--Import materialize.css-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/> <!--Let browser know website is optimized for mobile--> 
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>  <!--Import jQuery before materialize.js-->
  <script type="text/javascript" src="../../js/materialize.min.js"></script>  <!--Import jQuery before materialize.js-->

    <link  rel= "stylesheet"  href= "https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" > <!--Site de Icones-->



 <nav >
    <div class="nav-wrapper cyan" >
     <!-- <a href="index.php" class="brand-logo">SchoolPook </a>-->
      <a href="#" data-activates="slide-out" class="button-collapse show-on-large"><i class="mdi-navigation-menu">Menu</i></a>

<ul id="slide-out" class="side-nav  blue-grey darken-4">
     <li class="grey lighten-5" ><img src="http://i.imgsafe.org/ce5f242.png"height="100"></li>
    <li><i class="material-icons left">web</i><a href="../../View/alunos/index.php" class="white-text"> Pagina Inicial </a></li>
    <li><i class="material-icons left">perm_identity</i><a class="dropdown-button white-text" href="#!" data-activates="dropdown2">Usuarios<i class="mdi-navigation-arrow-drop-down right"></i></a></li>
    <li><i class="material-icons left">supervisor_account</i><a class="dropdown-button white-text " href="#!" data-activates="dropdown3" >Funcionarios<i class="mdi-navigation-arrow-drop-down right " ></i></a></li>
    <li><i class="material-icons left">shopping_basket</i><a class="dropdown-button white-text " href="#!" data-activates="dropdown4" >Produtos<i class="mdi-navigation-arrow-drop-down right " ></i></a></li>
    <li><i class="material-icons left">shopping_cart</i><a class="dropdown-button white-text " href="#!" data-activates="dropdown5" >Compras<i class="mdi-navigation-arrow-drop-down right " ></i></a></li>
  </ul>

      <ul id='dropdown2' class='dropdown-content cyan'>
      <li><a href="../../View/alunos/add.php"  class="white-text">Cadastre-se</a></li>
      <li><a href="../../View/alunos/dados.php" class="white-text">Usuario</a></li>
    </ul>

    <ul id='dropdown3' class='dropdown-content cyan'>
      <li><a href="../../View/funcionarios/add.php" class="white-text">Cadastre-se</a></li>
      <li><a href="../../View/funcionarios/dados.php" class="white-text">Funcionario</a></li> 
    </ul>

 <ul id='dropdown4' class='dropdown-content cyan'>
      <li><a href="../../View/produtos/add.php" class="white-text">Cadastre-se</a></li>
      <li><a href="../../View/produtos/dados.php" class="white-text">Produto</a></li> 
    </ul>


 <ul id='dropdown5' class='dropdown-content cyan'>
      <li><a href="../../View/compras/add.php" class="white-text">Cadastre-se</a></li>
      <li><a href="../../View/compras/dados.php" class="white-text">Compra</a></li> 
    </ul>




      <ul class="right hide-on-med-and-down">
        <li><a href="http://www.fieb.org.br/senai/"><i class="material-icons left">perm_identity</i>Contratante</a></li>
        <li><a href="badges.html"><i class="material-icons right">view_module</i>Link with Right Icon</a></li>
      </ul>
      
    </div>
  </nav>


<!-- Dropdown Structure -->

 <script> 
          $(document).ready(function(){
            $('.button-collapse').sideNav();
          });
          </script>