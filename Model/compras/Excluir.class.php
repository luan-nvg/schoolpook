<?php


class Excluir {

    private $idCompras;
    private $idAluno;
    private $idFuncionario;
    private $idProduto;
    private $dblTotal;
    private $intQuantCompras;

    public function __construct(){
        //echo "Acabei de criar uma classe!!";
        
    }
    
    public function getId(){
        return $this->idProduto;
    }
    
    public function getIdCompras(){
        return $this->idCompras;
    }
    
    public function getTotal(){
        return $this->dblTotal;
    }

    public function getIdAluno(){
        return $this->idAluno;
    }

    public function getIdFuncionario(){
        return $this->idFuncionario;
    }
    
    public function getQuantidadeCompras(){
        return $this->intQuantCompras;
    }

    public function setTotal($pTotal){
        $this->dblTotal = $pTotal;
    } 
    
    public function setIdAluno($pIdAluno){
        $this->idAluno = $pIdAluno;
    } 
    
    public function setIdProduto($pIdProduto){
        $this->idProduto = $pIdProduto;
    }     

    public function setIdCompras($pIdCompras){
        $this->idCompras = $pIdCompras;
    }


    public function setidFuncionario($pIdFuncionario){
        $this->idFuncionario = $pIdFuncionario;
    }           

    public function setQuantidadeCompras($pQuantidadeCompras){
        $this->intQuantCompras = $pQuantidadeCompras;
    }
    
}



?>