<?php


class Produtos {

    private $idProduto;
    private $intCodigo;
    private $vchNomeProduto;
    private $dblValor;
    private $intQuantidade;

    public function __construct(){
        //echo "Acabei de criar uma classe!!";
        
    }
    
    public function getId(){
        return $this->idProduto;
    }
    
    public function getNome(){
        return $this->vchNomeProduto;
    }
    
    public function getValor(){
        return $this->dblValor;
    }

    public function getCodigo(){
        return $this->intCodigo;
    }
    
    public function getQuantidade(){
        return $this->intQuantidade;
    }

    public function setValor($pValor){
        $this->dblValor = $pValor;
    } 
    
    public function setNome($pNome){
        $this->vchNomeProduto = $pNome;
    } 
    
    public function setId($pId){
        $this->idProduto = $pId;
    }

    public function setQuantidade($pQuantidade){
        $this->intQuantidade = $pQuantidade;
    }           

    public function setCodigo($pCodigo){
        $this->intCodigo = $pCodigo;
    }
    
}



?>