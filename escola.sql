-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 22-Dez-2015 às 00:49
-- Versão do servidor: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `escola`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `alunos`
--

CREATE TABLE IF NOT EXISTS `alunos` (
  `idAluno` int(11) NOT NULL AUTO_INCREMENT,
  `vchNome` varchar(100) NOT NULL,
  `vchTelefone` varchar(20) NOT NULL,
  `vchEmaill` varchar(100) DEFAULT NULL,
  `vchCPF` varchar(16) NOT NULL,
  `bolSexo` tinyint(1) NOT NULL,
  `tnyEstadoCivil` tinyint(4) NOT NULL,
  `intMatricula` int(11) DEFAULT NULL,
  `vchFile` mediumtext NOT NULL,
  `tnyEstado` tinyint(4) NOT NULL,
  `tnyCidade` tinyint(4) NOT NULL,
  PRIMARY KEY (`idAluno`),
  KEY `index2` (`idAluno`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='minha primeira tabela' AUTO_INCREMENT=432293 ;

--
-- Extraindo dados da tabela `alunos`
--

INSERT INTO `alunos` (`idAluno`, `vchNome`, `vchTelefone`, `vchEmaill`, `vchCPF`, `bolSexo`, `tnyEstadoCivil`, `intMatricula`, `vchFile`, `tnyEstado`, `tnyCidade`) VALUES
(432280, 'luan Cerqueira', '(71) 8543-9973', 'luan-nvg@hotmail.com', '93829243839428', 2, 1, 2015432280, '../../uploads/11053206_788668294573883_5170169965450631078_n.jpg', 1, 1),
(432281, 'Agua', '34432432', 'lucas@hotmail.com', '342423423', 1, 3, 2015432281, '../../uploads/Jellyfish.jpg', 1, 1),
(432287, 'Koala', '39482398', 'Koala@hotmail.com', '9834923898', 2, 4, 2015432287, '../../uploads/Koala.jpg', 1, 1),
(432288, 'flor', '5435938498', 'flor@hotmail.com', '4234234', 1, 1, 2015432288, '../../uploads/Hydrangeas.jpg', 1, 1),
(432290, 'Loreal', '34423432', 'loreal@hotmail.com', '4343', 1, 4, 2015432290, '../../uploads/Koala.jpg', 1, 1),
(432291, 'segundo', '34343', 'segundo@hotmail.com', '98439843', 2, 2, 2015432291, '../../uploads/Sfoto.jpg', 1, 3),
(432292, 'segundo', '34423432', 'estado@hotmail.com', '94389438', 1, 2, 2015432292, '../../uploads/Sfoto.jpg', 2, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cidade`
--

CREATE TABLE IF NOT EXISTS `cidade` (
  `nome_cidade` varchar(20) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `cidade`
--

INSERT INTO `cidade` (`nome_cidade`, `id`, `estado`) VALUES
('Salvador', 1, 1),
('Rio de Janeiro', 2, 2),
('Cajazeiras', 3, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `compras`
--

CREATE TABLE IF NOT EXISTS `compras` (
  `idCompras` int(11) NOT NULL AUTO_INCREMENT,
  `idAluno` int(11) NOT NULL,
  `idFuncionario` int(11) NOT NULL,
  `idProduto` int(11) NOT NULL,
  `dblValor` double NOT NULL,
  PRIMARY KEY (`idCompras`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `estado`
--

CREATE TABLE IF NOT EXISTS `estado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sigla` varchar(2) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `estado`
--

INSERT INTO `estado` (`id`, `sigla`, `descricao`) VALUES
(1, 'BA', 'Bahia'),
(2, 'RJ', 'Rio de Janeiro');

-- --------------------------------------------------------

--
-- Estrutura da tabela `estadocivil`
--

CREATE TABLE IF NOT EXISTS `estadocivil` (
  `tnyEstado` tinyint(4) NOT NULL AUTO_INCREMENT,
  `vchDescricao` varchar(100) NOT NULL,
  PRIMARY KEY (`tnyEstado`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `estadocivil`
--

INSERT INTO `estadocivil` (`tnyEstado`, `vchDescricao`) VALUES
(1, 'solteiro'),
(2, 'casado'),
(3, 'viuvo'),
(4, 'divorciado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionarios`
--

CREATE TABLE IF NOT EXISTS `funcionarios` (
  `idFuncionario` int(11) NOT NULL AUTO_INCREMENT,
  `vchNome` varchar(100) NOT NULL,
  `vchTelefone` varchar(20) NOT NULL,
  `vchEmaill` varchar(100) DEFAULT NULL,
  `vchCPF` varchar(16) NOT NULL,
  `bolSexo` tinyint(1) NOT NULL,
  `tnyEstadoCivil` tinyint(4) NOT NULL,
  `intCodigoFuncionario` int(11) DEFAULT NULL,
  `vchFile` mediumtext NOT NULL,
  `tnyEstado` tinyint(4) NOT NULL,
  `tnyCidade` tinyint(4) NOT NULL,
  `intQuantidadeVendas` int(11) NOT NULL,
  PRIMARY KEY (`idFuncionario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='minha primeira tabela' AUTO_INCREMENT=131 ;

--
-- Extraindo dados da tabela `funcionarios`
--

INSERT INTO `funcionarios` (`idFuncionario`, `vchNome`, `vchTelefone`, `vchEmaill`, `vchCPF`, `bolSexo`, `tnyEstadoCivil`, `intCodigoFuncionario`, `vchFile`, `tnyEstado`, `tnyCidade`, `intQuantidadeVendas`) VALUES
(107, 'lol', '43324', 'leonardo@homo.com', '354049', 1, 1, 2015107, 'uploads/Sfoto.jpg', 1, 1, 0),
(109, 'jkfdsd', 'dsfsdfsda', 'sdad', 'adsdad', 1, 1, 2015109, 'uploads/Sfoto.jpg', 1, 1, 0),
(110, 'pancretas', '433434', 'lpo@hotal.com', '34344343', 1, 1, 2015110, 'uploads/Penguins.jpg', 1, 1, 0),
(111, 'locale', '343', 'locale@hotmai.com', '4343434323423', 1, 1, 2015111, 'uploads/Hydrangeas.jpg', 1, 1, 0),
(113, 'casinh', '434343', 'casinha@hotmail.com', '0439842309823', 1, 1, 2015113, 'uploads/Koala.jpg', 1, 1, 0),
(114, 'casa', '4344398', 'casa@hotmail.com', '9889', 1, 1, 2015114, 'uploads/Tulips.jpg', 1, 1, 0),
(117, 'LOl', '3443943', 'loL@hotmail.com', '3442378432', 1, 1, 2015117, 'uploads/Penguins.jpg', 1, 1, 0),
(118, 'ppoopo', '43434387', 'popO@hotmil.com', '8437843743', 1, 1, 2015118, 'uploads/Penguins.jpg', 1, 1, 0),
(119, 'noe', '34343', 'noe@hotmail.com', '4343433434', 1, 1, 2015119, 'uploads/Sfoto.jpg', 1, 1, 0),
(120, 'index', '430403944239', 'index', '0090490439', 1, 1, 2015120, 'uploads/Sfoto.jpg', 1, 1, 0),
(122, 'manga', '438437843787', 'manga@hotmail.com', '84378437348', 1, 1, 2015122, 'uploads/Sfoto.jpg', 1, 1, 0),
(123, 'Lumier', '34423842398', 'lumier3@hotmail.com', '84398439483', 1, 1, 2015123, 'uploads/Sfoto.jpg', 1, 1, 0),
(124, 'neymar', '34329843289', 'neym@hotmail.com', '89438943834', 1, 1, 2015124, 'uploads/Lighthouse.jpg', 1, 1, 0),
(125, 'pollaa', '34342423', 'pop@hotmal.com', '2442332894283', 1, 1, 2015125, 'uploads/Hydrangeas.jpg', 1, 1, 0),
(126, 'LOl', '8427428378', 'loL@hoymilcom', '8743843787', 1, 1, 2015126, 'uploads/Sfoto.jpg', 1, 1, 0),
(127, 'negao', '3948398', 'negao', '989438', 1, 3, 2015127, 'uploads/Lighthouse.jpg', 1, 1, 0),
(128, 'Fabio', '49384398', 'fabio@hotmail.com', '94389438', 1, 3, 2015128, '../../uploads/Sfoto.jpg', 1, 1, 0),
(129, 'segundo', '4343', 'segundo@hotmail.com', '0943094309', 0, 1, 2015129, '../../uploads/Sfoto.jpg', 2, 2, 0),
(130, 'mingual', '0439439', 'lol@hotmail.com', '4343', 0, 1, 2015130, '../../uploads/Sfoto.jpg', 1, 3, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE IF NOT EXISTS `produtos` (
  `idProduto` int(11) NOT NULL AUTO_INCREMENT,
  `intCodigo` int(11) NOT NULL,
  `vchNomeProduto` varchar(30) NOT NULL,
  `dblValor` double NOT NULL,
  `intQuantidade` int(11) NOT NULL,
  `vchFile` varchar(30) NOT NULL,
  PRIMARY KEY (`idProduto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
